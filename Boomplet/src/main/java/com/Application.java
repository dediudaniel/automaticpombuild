package com;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;
import java.util.Properties;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.model.Configuration;

@SpringBootApplication
public class Application {
	
	
	public static Properties properties;
		
	
	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
		properties();
	}
	
	public static void properties(){
		try {
			File file = new File("config.properties");
			FileInputStream fileInput = new FileInputStream(file);
			properties = new Properties();
			properties.load(fileInput);
			fileInput.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public static void updateProperties(List<Configuration> configuration){
		try {
			
			BufferedWriter bw = new BufferedWriter(new FileWriter("config.properties"));
            StringBuilder data = new StringBuilder();
            for(Configuration config : configuration){
            	data.append(config.getKey()).append("=").append(config.getValue().replace("\\", "/")).append("\n");
            }
            bw.write(data.toString());
            bw.close();
            properties();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
