package com.controller;


import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import com.Application;

@Component
public class FileScanner {
	
	private String fileNameToSearch;
	private List<String> result = new ArrayList<String>();

	public String getFileNameToSearch() {
		return fileNameToSearch;
	}

	public void setFileNameToSearch(String fileNameToSearch) {
		this.fileNameToSearch = fileNameToSearch;
	}

	public List<String> getResult() {
		return result;
	}

	public List<String> init() throws IOException {

		FileScanner fileSearch = new FileScanner();

		fileSearch.searchDirectory(new File(Application.properties.getProperty("workspace")), "pom.xml");

		int count = fileSearch.getResult().size();
		if(count ==0){
			System.out.println("\nNo result found!");
		}else{
			for (String matched : fileSearch.getResult()){
				System.out.println("Found : " + matched);
			}
		}
		return fileSearch.getResult();
	}

	public void searchDirectory(File directory, String fileNameToSearch) throws IOException {

		setFileNameToSearch(fileNameToSearch);

		if (directory.isDirectory()) {
			search(directory);
		} else {
			System.out.println(directory.getAbsoluteFile() + " is not a directory!");
		}

	}

	private void search(File file) throws IOException {

		if (file.isDirectory()) {

			if (file.canRead()) {
				if(file.getCanonicalPath().contains("target")){
					return;
				}
				for (File temp : file.listFiles()) {
					if (temp.isDirectory()) {
						search(temp);
					} else {
						if (getFileNameToSearch().equals(temp.getName().toLowerCase())) {
							result.add(file.getAbsolutePath());
						}
					}
				}

			} else {
				System.out.println(file.getAbsoluteFile() + "Permission Denied");
			}
		}

	}

}