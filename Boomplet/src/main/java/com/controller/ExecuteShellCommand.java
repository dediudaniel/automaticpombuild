package com.controller;

import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintStream;
import java.util.List;
import java.util.Map;

import com.Application;
import com.model.Task;

public class ExecuteShellCommand {

	public static void generateBat(List<Task> tasks){

		try (BufferedWriter bw = new BufferedWriter(new FileWriter(Application.properties.getProperty("batfile")))) {

			StringBuilder data =new StringBuilder();
			for(Task task : tasks){
				if(task.getBuild()){
					data.append("\n cd /d ").append("\""+task.getTitle()+"\"")
						.append("\n cmd /c ").   append(task.getCommand() != null ? task.getCommand() : "" )
						.append("\n cd /d ").append("\""+task.getTitle()+"\"")
						.append("\n cmd /c ")   .append(task.getAlternativeCommand() != null ? task.getAlternativeCommand() :"")
						.append("\n cd /d ").append("\""+task.getTitle()+"\\target")
						.append("\n set \"source="+task.getTitle()+"\\target\"" +
								"\n set \"dest="+Application.properties.getProperty("deploypath")+"\"" +
								"\n pushd \"%source%\" ||(echo.Source does not exist&pause&goto EOF)"+
								"\n for /f \"tokens=*\" %%f in ('dir /A-D /OD /B') Do set \"file=%%f\" "+
								"\n popd" +
								"\n xcopy /d /i \"%source%\\%file%\" \"%dest%\" /Y");
				}
			}
			bw.write(data.toString());
		} catch (IOException e) {

			e.printStackTrace();

		}
	}
	public static String init() throws InterruptedException, IOException {
		ProcessBuilder pb = new ProcessBuilder(Application.properties.getProperty("batfile"));
		Map<String, String> env = pb.environment();

		final ByteArrayOutputStream myOut = new ByteArrayOutputStream();
		System.setOut(new PrintStream(myOut));
		Process process = pb.start();
		process.waitFor();
		pb = new ProcessBuilder("cmd");
		pb.environment().putAll(env);
		pb.inheritIO();
		pb.redirectInput(new File("src/resources/ping.bat"));
		process = pb.start();
		process.waitFor();
		final String standardOutput = myOut.toString();
	    return standardOutput;

	}

}