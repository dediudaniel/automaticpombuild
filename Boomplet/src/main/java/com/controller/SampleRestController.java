package com.controller;



import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Map.Entry;

import javax.servlet.ServletContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.Application;
import com.model.Configuration;
import com.model.SaveProfile;
import com.model.Task;

@RestController
public class SampleRestController {
	
	 @Autowired
     ServletContext context;

	private static List<Task> list;


	@PostMapping("/save")
	public String save(@RequestBody SaveProfile profile){
		try {
			String value = context.getRealPath("/");
			value = value.replace("\\", "/");
			Integer i = value.indexOf("src");
			String newPath = value.substring(0, i);
			profile.setProfileName(profile.getProfileName().replace(".profile", ""));
			String path = newPath + Application.properties.getProperty("saveprofile")+profile.getProfileName()+".profile";
			File file = new File(path);
			file.createNewFile();
			FileOutputStream fileOut =	new FileOutputStream(path);
			ObjectOutputStream out = new ObjectOutputStream(fileOut);
			out.writeObject(profile.getTasks());
			out.close();
			fileOut.close();
		}catch(IOException i) {
			i.printStackTrace();
		}
		return null;
	}
	
	@PostMapping("/delete")
	public String delete(@RequestBody String profile){
		deleteProfile(profile);
		return null;
	}
	

	@SuppressWarnings("unchecked")
	@PostMapping("/load")
	public List<Task> load(@RequestBody String profileName){
		List<Task> list = null;
		try {
			FileInputStream fileIn = new FileInputStream(Application.properties.getProperty("saveprofile")+profileName);
			ObjectInputStream in = new ObjectInputStream(fileIn);
			list =  (ArrayList<Task>) in.readObject();
			in.close();
			fileIn.close();
		}catch(IOException i) {
			i.printStackTrace();
		}catch(ClassNotFoundException c) {
			c.printStackTrace();
		}
		return list;
	}


	@PostMapping("/buildLog")
	public String generateLog(@RequestBody List<Task> tasks) {
		return procesTasks(tasks);
	}
	
	@PostMapping("/updateConfiguration")
	public String updateConfiguration(@RequestBody List<Configuration> configuration) {
		return procesConfiguration(configuration);
	}
	
	@GetMapping("/loadProfiles")
	public List<String>  loadProfiles() throws IOException{
		return setLoadProfiles();
	}
	
	@GetMapping("/getList")
	public List<Task>  hello() throws IOException{
		return setTask();
	}
	
	@GetMapping("/getConfiguration")
	public List<Configuration>  getConfigurationn() throws IOException{
		return setConfiguration();
	}
	
	
	private List<Configuration> setConfiguration() throws IOException{
		 List<Configuration> list = new ArrayList<Configuration>();
		 for(Entry<Object, Object> e : Application.properties.entrySet()) {
	            list.add(new Configuration(e));
	        }
		 return list;
	}

	private String procesConfiguration(List<Configuration> configuration){
		Application.updateProperties(configuration);
		return "";
	}
	

	private String procesTasks(List<Task> tasks){
		String log = null;
		ExecuteShellCommand.generateBat(tasks);
		try {
			String value = context.getRealPath("/");
			value = value.replace("\\", "/");
			Integer i = value.indexOf("src");
			String newPath = value.substring(0, i);
			String path = newPath + Application.properties.getProperty("batfile");

			
			Runtime.getRuntime().exec("cmd /c start "+path);
//			log = ExecuteShellCommand.init();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return log;

	}
	
	private List<String> setLoadProfiles() throws IOException{
		List<String> result = new ArrayList<String>();
		String value = context.getRealPath("/");
		value = value.replace("\\", "/");
		Integer i = value.indexOf("src");
		String newPath = value.substring(0, i);
		String path = newPath + Application.properties.getProperty("saveprofile");
		File file = new File(path);
		for (File temp : file.listFiles()) {
			if(temp.getName().contains(".profile")){
				result.add(temp.getName());
			}
		}
		
		
		return result;
	}
	
	private void deleteProfile(String profileName){
		String value = context.getRealPath("/");
		value = value.replace("\\", "/");
		Integer i = value.indexOf("src");
		String newPath = value.substring(0, i);
		String path = newPath + Application.properties.getProperty("saveprofile");
		File file = new File(path);
		for (File temp : file.listFiles()) {
			if(temp.getName().contains(profileName)){
				temp.delete();
				break;
			}
		}
	}
	
	private List<Task> setTask() throws IOException{
		if(list == null){

			List<Task> result = new ArrayList<Task>();
			FileScanner scanner = new FileScanner();
			int id = 1;
			for(String command:scanner.init()){
				Task task = new Task();

				task.setId(id);
				task.setTitle(command);
				task.setBuild(true);
				id++;
				result.add(task);
				list = result;
			}
			return result;
		}else{
			return list;
		}

	}





}
