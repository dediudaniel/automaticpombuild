package com.model;

import java.io.Serializable;

public class Task implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -7518700242137436621L;
	private Integer id;
	private String title;
	private Boolean build;
	private String command;
	private String alternativeCommand;
	

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getCommand() {
		return command;
	}

	public void setCommand(String command) {
		this.command = command;
	}

	public String getAlternativeCommand() {
		return alternativeCommand;
	}

	public void setAlternativeCommand(String alternativeCommand) {
		this.alternativeCommand = alternativeCommand;
	}

	public Boolean getBuild() {
		return build;
	}

	public void setBuild(Boolean build) {
		this.build = build;
	}
}
