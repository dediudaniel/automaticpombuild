package com.model;

import java.io.Serializable;
import java.util.Map.Entry;

public class Configuration implements Serializable {
	/**
	* 
	*/
	private static final long serialVersionUID = 1L;
	private String key;
	private String value;
	
	public Configuration(){}
	public Configuration(String key, String value){
		this.key = key;
		this.value = value;
	}
	public Configuration(Entry<Object, Object> e) {
		this.key = (String) e.getKey();
		this.value = (String) e.getValue();
	}
	public String getKey() {
		return key;
	}
	public void setKey(String key) {
		this.key = key;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	
}
