package com.model;

import java.util.ArrayList;

public class SaveProfile {
	private String profileName;
	private ArrayList<Task> tasks;
	public String getProfileName() {
		return profileName;
	}
	public void setProfileName(String profileName) {
		this.profileName = profileName;
	}
	public ArrayList<Task> getTasks() {
		return tasks;
	}
	public void setTasks(ArrayList<Task> tasks) {
		this.tasks = tasks;
	}
}
